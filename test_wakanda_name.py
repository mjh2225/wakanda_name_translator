import unittest
import wakanda_name

class TestWakandaName(unittest.TestCase):

    def test_get_masculine_name(self):
        output = wakanda_name.get_masculine_name("William")
        self.assertEqual(output, "W'Tambi")

    def test_get_feminine_name(self):
        output = wakanda_name.get_feminine_name("Sherry")
        self.assertEqual(output, "Shuri")

    def test_translate_name(self):
        output = wakanda_name.translate_name("William", "W'Tambi")
        self.assertEqual(output, "W'Lalmi")

        output = wakanda_name.translate_name("Sherry", "Shuri")
        self.assertEqual(output, "Shuri")




if __name__ == '__main__':
    unittest.main(verbosity=2)
