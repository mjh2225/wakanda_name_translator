# [Wakandan Name Translator](https://bitbucket.org/mjh2225/wakanda_name_translator/src/master/)

## Description

_Wakandan Name Translator_ is a program to translate names to the style of indigenous names of the fictional country of Wakanda. Inspired by the Black Panther movie and comic book.

For example ...

    Enter your name: William

And this returns:

    gender: male
    most_similar_name W'Tambi
    wakandan_name:  W'Lalmi
    

## Installation
_Wakandan Name Translator_ is written to work in Python 3.x  
  git clone https://mjh2225@bitbucket.org/mjh2225/wakanda_name_translator.git  
  pip install -r requirements.txt

## Usage
```python
python wakanda_name.py
```
